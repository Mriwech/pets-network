<!DOCTYPE html>
<html>
@include('frontOffice.backEnd.inc.head', ['meta' => ''])
<body>
@include('sweet::alert')
@include('frontOffice.backEnd.inc.header')

<div id="page-contents">
    <div class="container">
        <div class="row">
            @include('frontOffice.backEnd.inc.leftSide')
            @yield('content')
            @include('frontOffice.backEnd.inc.rightSide')
        </div>
    </div>
</div>

@include('frontOffice.backEnd.inc.footer')
@yield('js')
@include('frontOffice.backEnd.inc.scripts')
</body>
</html>