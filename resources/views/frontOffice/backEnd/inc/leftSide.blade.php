<!-- Newsfeed Common Side Bar Left
                ================================================= -->
<div class="col-md-3 static">
    <div class="profile-card">
        <img src="{{ asset(Auth::user()->image_path) }}" alt="user" class="profile-photo" />
        <h5>
            <a href="{{ route('showUserAccueil') }}" class="text-white">
                @if(Auth::user()) {{ Auth::user()->nom }} {{ Auth::user()->prenom  }} @endif
            </a>
        </h5>
    </div><!--profile card ends-->
    <ul class="nav-news-feed">
        <li><i class="icon ion-ios-home"></i><div><a href="{{ route('showUserAccueil') }}">Accueil</a></div></li>
        <li><i class="icon ion-ios-paper"></i><div><a href="{{ route('showUserUpdateProfile', ['id' => Auth::user()->id])  }}">Editer profile</a></div></li>
        <li><i class="icon ion-ios-people"></i><div><a href="{{ route('showUserFollows') }}">Abonnements</a> <span class="bg-success">({{ Auth::user()->follows()->count() }})</span></div></li>
        <li><i class="icon ion-ios-paw"></i><div><a href="{{ route('showUserManageAnimaux') }}">Mes animaux</a> <span class="bg-success">({{ Auth::user()->animaux()->count() }})</span></div></li>
        <li><i class="icon ion-images"></i><div><a href="{{ route('showUserImages') }}">Mes images</a> <span class="bg-success">({{ Auth::user()->photos()->count() }})</span></div></li>
        <li><i class="icon ion-android-add-circle"></i><div><a href="{{ route('showManageComments') }}">Gérer mes commentaires</a> <span class="bg-success">({{ nbrCommentaires() }})</span></div></li>
    </ul><!--news-feed links ends-->
</div>