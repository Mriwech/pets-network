<head>
    <meta charset="utf-8">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="This is social network html5 template available in themeforest......" />
    <meta name="keywords" content="Social Network, Social Media, Make Friends, Newsfeed, Profile Page" />
    <meta name="robots" content="index, follow" />
    <title>Pets Network | By Marouéne TAYARI</title>

    <!-- Stylesheets
    ================================================= -->
    <link rel="stylesheet" href="{{ asset('/') }}css/bootstrap.min.css" />
    <link rel="stylesheet" href="{{ asset('/') }}css/style.css" />
    <link rel="stylesheet" href="{{ asset('/') }}css/ionicons.min.css" />
    <link rel="stylesheet" href="{{ asset('/') }}css/font-awesome.min.css" />

    <!--Google Font-->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700,700i" rel="stylesheet">

    <!--Favicon-->
    <link rel="shortcut icon" type="image/png" href="{{ asset('/') }}images/fav.png"/>

    <!--begin::Sweet Alert-->
    <link rel="stylesheet" type="text/css" href="{{asset('plugins/sweetalert/sweetalert.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('plugins/sweetalert/swal-forms.css')}}">
    <script src="{{asset('plugins/sweetalert/sweetalert2.min.js')}}"></script>

    <!-- Select2 -->
    <link href="{{asset('/')}}css/select2.min.css" rel="stylesheet" />



    @yield('css')
</head>