<!-- Scripts
    ================================================= -->
<script src="{{asset('/')}}js/jquery-3.1.1.min.js"></script>
<script src="{{asset('/')}}js/bootstrap.min.js"></script>
<script src="{{asset('/')}}js/jquery.appear.min.js"></script>
<script src="{{asset('/')}}js/jquery.incremental-counter.js"></script>
<script src="{{asset('/')}}js/script.js"></script>

<!-- Select2 -->
<script src="{{asset('/')}}js/select2.min.js"></script>

@yield('js')