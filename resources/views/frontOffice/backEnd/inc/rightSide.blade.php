<!-- Newsfeed Common Side Bar Right
                ================================================= -->
<div class="col-md-2 static">
    <div class="suggestions" id="sticky-sidebar">
        @if(count(animauxAleatoire()) > 0)
            <h4 class="grey">Animaux aléatoire</h4>
        @endif
        @foreach(animauxAleatoire() as $animal)
            <div class="follow-user">
                <img src="{{ asset($animal->image_path) }}" alt="" class="profile-photo-sm pull-left" />
                <div>
                    <h5><a href="{{ route('showAnimalProfile', ['id' => $animal->id]) }}">{{ $animal->nom }}</a></h5>
                    @if(suivireAnimal($animal->id))
                        <a href="{{route('handleUserUnfollowAnimal', ['animalId' => $animal->id])}}" class="text-red">Désabonner</a>
                    @else
                        <a href="{{route('handleUserFollowAnimal', ['animalId' => $animal->id])}}" class="text-green">S'abonner</a>
                    @endif
                </div>
            </div>
        @endforeach

    </div>
</div>