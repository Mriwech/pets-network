<!DOCTYPE html>
<html>
@include('frontOffice.backEnd.inc.head', ['meta' => ''])
<body>
@include('sweet::alert')
{{--@include('frontOffice.backEnd.inc.headerProfile')--}}

<div class="container">
    <div class="timeline">
        <div class="timeline-cover">

            <!--Timeline Menu for Large Screens-->
            <div class="timeline-nav-bar hidden-sm hidden-xs">
                <div class="row">
                    <div class="col-md-3">
                        <div class="profile-info">
                            <img src="{{ asset($animal->image_path) }}" alt="" class="img-responsive profile-photo"/>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <h4>{{ $animal->nom }}</h4>
                        <h5 class="text-white">Proprietaire : {{ $animal->proprietaire->nom.', '.$animal->proprietaire->prenom }}</h5>
                        <ul class="follow-me list-inline">
                            <li>{{ nbrOfFollowers($animal->id) }} followers</li>
                            <li>
                                @if(\Illuminate\Support\Facades\Auth::user()->id != $animal->user_id)
                                    @if(suivireAnimal($animal->id))
                                        <a href="{{route('handleUserUnfollowAnimal', ['animalId' => $animal->id])}}"><button class="btn-primary" style="background-color: darkred;">Désabonner</button></a>
                                    @else
                                        <a href="{{route('handleUserFollowAnimal', ['animalId' => $animal->id])}}"><button class="btn-primary">S'abonner</button></a>
                                    @endif
                                @endif
                            </li>
                        </ul>
                    </div>
                </div>
            </div><!--Timeline Menu for Large Screens End-->
        </div>
        <div id="page-contents">
            <div class="row">
                {{--<div class="col-md-3"></div>--}}

                @include('frontOffice.backEnd.inc.leftSide')
                <div class="col-md-2 static"></div>
                @yield('content')


            </div>
        </div>
    </div>
</div>

@include('frontOffice.backEnd.inc.footer')
@yield('js')
@include('frontOffice.backEnd.inc.scripts')
</body>
</html>