<!DOCTYPE html>
<html>
@include('frontOffice.frontEnd.inc.head', ['meta' => ''])
<body>
@include('sweet::alert')
@yield('header')
@yield('content')
@include('frontOffice.frontEnd.inc.scripts')
</body>
</html>