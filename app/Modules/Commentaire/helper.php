<?php

/**
 *	Commentaire Helper
 */

if (!function_exists('etatCommentaire')) {
    /**
     * @param \App\Modules\User\Models\User $user
     * @return bool
     */
    function etatCommentaire($etat)
    {
        switch ($etat){
            case 0 : return "Non valid";
            case 1 : return "Validé";
            default : return "Supprimer";
        }

    }
}

if (!function_exists('nbrCommentaires')) {
    /**
     * @param \App\Modules\User\Models\User $user
     * @return bool
     */
    function nbrCommentaires()
    {
        $photos = \Illuminate\Support\Facades\Auth::user()->photos()->get();

        $nbrTotal = 0;
        foreach ($photos as $photo){
            $nbrTotal += $photo->commentairesInvalid->count();
        }
        return $nbrTotal;
    }
}