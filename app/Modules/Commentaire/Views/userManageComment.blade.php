@extends('frontOffice.backEnd.layoutWithoutRightSide')


@section('content')

    <div class="col-md-9">

        <!-- Basic Information
        ================================================= -->
        <center class="friend-list">
            <div class="block-title">
                <h4 class="grey"><i class="icon ion-android-checkmark-circle"></i> Liste des commentaires</h4>
                <div class="line"></div>
            </div>
            <table id="example" class="table table-striped table-bordered" style="width:100%;text-align: center;">
                <thead>
                <tr>
                    <th>Photo</th>
                    <th>User</th>
                    <th>Commentaire</th>
                    <th>Date</th>
                    <th>Etat</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach(\Illuminate\Support\Facades\Auth::user()->photos()->get() as $photo)
                    @foreach($photo->allCommentaires()->get() as $commentaire)
                    <tr>
                        <td><img src="{{ asset($photo->image_path) }}"  height="100"></td>
                        <td>{{ $commentaire->proprietaire->nom .', '.$commentaire->proprietaire->prenom }}</td>
                        <td>{{ $commentaire->commentaire }}</td>
                        <td>{{ $commentaire->created_at }}</td>
                        <td>{{ etatCommentaire($commentaire->etat) }}</td>
                        <td>
                            @if($commentaire->etat == 0)
                                <a href="{{ route('handleChangeCommentStatus', ['id' => $commentaire->id, 'etat' => 1]) }}"><button class="btn bg-success" title="Valider"><i class="fa fa-check"></i></button></a>
                            @elseif($commentaire->etat == 1)
                            <a href="{{ route('handleChangeCommentStatus', ['id' => $commentaire->id, 'etat' => 2]) }}"><button class="btn bg-danger" title="Supprimer"><i class="fa fa-close"></i></button></a>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                @endforeach
                </tbody>
                <tfoot>
                <tr>
                    <th>Photo</th>
                    <th>User</th>
                    <th>Commentaire</th>
                    <th>Date</th>
                    <th>Etat</th>
                    <th>Action</th>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>

@endsection

@section('js')
    <script>
        $(document).ready(function() {
            $('#example').DataTable();
        } );
    </script>
@endsection





