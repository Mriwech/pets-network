<?php

namespace App\Modules\Commentaire\Models;

use Illuminate\Database\Eloquent\Model;

class Commentaire extends Model {

    public $timestamps = true;

    protected $table = 'commentaires';

    protected $fillable = ['commentaire', 'etat', 'photo_id', 'user_id'];

    public function proprietaire()
    {
        return $this->hasOne('App\Modules\User\Models\User', 'id', 'user_id');
    }

}
