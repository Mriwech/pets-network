<?php

Route::group(['module' => 'Commentaire', 'middleware' => ['api'], 'namespace' => 'App\Modules\Commentaire\Controllers'], function() {

    Route::resource('Commentaire', 'CommentaireController');

});
