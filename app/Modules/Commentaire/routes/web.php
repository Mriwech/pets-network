<?php

Route::group(['module' => 'Commentaire', 'middleware' => ['web'], 'namespace' => 'App\Modules\Commentaire\Controllers'], function() {

    Route::resource('Commentaire', 'CommentaireController');

    Route::prefix('comment')->group(function() {
       route::post('ajouter', 'CommentaireController@handleAddCommentaire')->name('handleAddCommentaire')->middleware('authenticate');
       route::get('manage', 'CommentaireController@showManageComments')->name('showManageComments')->middleware('authenticate');
       route::get('change/etat/{id}{etat}', 'CommentaireController@handleChangeCommentStatus')->name('handleChangeCommentStatus')->middleware('authenticate');
    });

});
