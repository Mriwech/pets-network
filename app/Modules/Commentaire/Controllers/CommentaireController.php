<?php

namespace App\Modules\Commentaire\Controllers;

use App\Modules\Commentaire\Models\Commentaire;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use UxWeb\SweetAlert\SweetAlert;

class CommentaireController extends Controller
{

    public function handleAddCommentaire(Request $request){
        Commentaire::create([
            'commentaire' => $request->commentaire,
            'photo_id' => $request->photoId,
            'user_id' => Auth::user()->id,
        ]);

        SweetAlert::success('Commentaire ajouté avec succès en attendant la validation par le propriétaire !', 'Success !')->persistent('Fermer');
        return back();
    }

    public function showManageComments(){
        return view('Commentaire::userManageComment');
    }

    public function handleChangeCommentStatus($id, $etat){
        Commentaire::where('id', $id)->update(['etat' => $etat]);
        return back();
    }
}
