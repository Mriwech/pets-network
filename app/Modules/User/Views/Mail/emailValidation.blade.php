<html>
<head></head>
<body>
Bonjour {{ $user->nom }}, {{ $user->prenom }}<br>

Merci de validez votre compte en cliquant sur ce lien : <a href="{{route('handleValidateUserAccount', ['token' => $user->validation_token])}}">valider</a>.
<br>

Si le lien ne s'affiche pas correctement merci de le coller dans votre barre d'adresse : <br>
<input type="text" style="width: 100%;" value="{{route('handleValidateUserAccount', ['token' => $user->validation_token])}}"/>

</body>
</html>