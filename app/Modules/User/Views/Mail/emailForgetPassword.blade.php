<html>
<head></head>
<body>
Bonjour,<br><br>

Vous avez récemment demandé à réinitialiser votre mot de passe pour votre compte.
Utilisez le bouton ci-dessous pour le réinitialiser. Cette réinitialisation du mot de passe n'est valide que pour les prochaines 24 heures.

<a href="{{route('handleUserPasswordReset', ['email' => $password->email, 'token' => $password->token])}}">
    <button>Reset password</button>
</a>
<br>

</body>
</html>