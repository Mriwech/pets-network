@extends('frontOffice.backEnd.layout')


@section('content')

    <div class="col-md-7">

        <!-- Basic Information
        ================================================= -->
        <div class="edit-profile-container">
            <div class="block-title">
                <h4 class="grey"><i class="icon ion-android-checkmark-circle"></i>Edit basic information</h4>
                <div class="line"></div>
            </div>
            <div class="edit-block">
                <form name="basic-info" id="basic-info" class="form-inline" action="{{route('handleUserUpdateProfile', ['user' => $user->id])}}" method="post">
                    @csrf
                    <div class="row">
                        <div class="form-group col-xs-6">
                            <label for="firstname">Nom</label>
                            <input id="firstname" class="form-control input-group-lg" type="text" name="nom" title="Enter first name" placeholder="Votre nom" @if($user->nom) value="{{$user->nom}}" @endif required>
                        </div>
                        <div class="form-group col-xs-6">
                            <label for="lastname" class="">Prénom</label>
                            <input id="lastname" class="form-control input-group-lg" type="text" name="prenom" title="Enter last name" placeholder="Votre prénom" @if($user->prenom) value="{{$user->prenom}}" @endif required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-xs-12">
                            <label for="email">Email</label>
                            <input id="email" class="form-control input-group-lg" type="text" name="email" title="Email" placeholder="Votre Email" @if($user->email) value="{{$user->email}}" @endif required readonly>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-xs-12">
                            <label for="email">Mot de passe</label>
                            <input id="password" class="form-control input-group-lg" type="password" name="password" title="Mot de passe" placeholder="Mot de passe">
                        </div>
                        <div class="form-group col-xs-12">
                            <label for="email">Confirmer mot de passe</label>
                            <input id="password" class="form-control input-group-lg" type="password" name="confPassword" title="Confirmer mot de passe" placeholder="Confirmer mot de passe">
                        </div>
                    </div>

                    <button type="submit" class="btn btn-primary">Modifier profile</button>
                </form>
            </div>
        </div>
    </div>

@endsection





