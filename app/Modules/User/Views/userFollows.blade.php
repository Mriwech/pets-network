@extends('frontOffice.backEnd.layout')


@section('content')
    <div class="col-md-7">

        <!-- Friend List
        ================================================= -->
        <div class="friend-list">
            <div class="row">
                @foreach(\Illuminate\Support\Facades\Auth::user()->follows()->get() as $animal)
                    <div class="col-md-6 col-sm-6">
                        <div class="friend-card">
                            <img src="{{ asset('images/bg/cover-2.jpg') }}" alt="profile-cover" class="img-responsive cover">
                            <div class="card-info">
                                <img src="{{ asset($animal->image_path) }}" alt="{{ $animal->nom }}" class="profile-photo-lg">
                                <div class="friend-info">
                                    <a href="#" class="pull-right text-green">{{ $animal->animal }}</a>
                                    <h5><a href="{{ route('showAnimalProfile', ['id' => $animal->id]) }}" class="profile-link">{{ $animal->nom }}</a></h5>
                                    <p>{{ $animal->description }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach

            </div>
        </div>
    </div>

@endsection





