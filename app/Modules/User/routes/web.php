<?php

Route::group(['module' => 'User', 'middleware' => ['web'], 'namespace' => 'App\Modules\User\Controllers'], function() {

    Route::resource('User', 'UserController');

    Route::prefix('user')->group(function() {
        Route::post('register', 'UserController@handleUserRegistration')->name('handleUserRegistration');
        Route::get('validate/{token}', 'UserController@handleValidateUserAccount')->name('handleValidateUserAccount');

        Route::post('login', 'UserController@handleLoginUser')->name('handleLoginUser');
        Route::get('logout', 'UserController@handleLogoutUser')->name('handleLogoutUser');

        Route::post('forgetPassword', 'UserController@handleUserForgetPassword')->name('handleUserForgetPassword');
        Route::get('password/reset/{email}/{token}', 'UserController@handleUserPasswordReset')->name('handleUserPasswordReset');


        Route::get('accueil', 'UserController@showUserAccueil')->name('showUserAccueil')->middleware('authenticate');
        Route::get('profile/{id}', 'UserController@showUserUpdateProfile')->name('showUserUpdateProfile')->middleware('authenticate');
        Route::post('profile/update', 'UserController@handleUserUpdateProfile')->name('handleUserUpdateProfile')->middleware('authenticate');

        Route::get('follows/show', 'UserController@showUserFollows')->name('showUserFollows')->middleware('authenticate');
        Route::get('follow/{animalId}', 'UserController@handleUserFollowAnimal')->name('handleUserFollowAnimal')->middleware('authenticate');
        Route::get('unfollow/{animalId}', 'UserController@handleUserUnfollowAnimal')->name('handleUserUnfollowAnimal')->middleware('authenticate');
    });

});
