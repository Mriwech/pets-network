<?php

/**
 *	User Helper
 */

use App\Modules\User\Models\UserFollows;

if (!function_exists('suivireAnimal')) {
    /**
     * @param \App\Modules\User\Models\User $user
     * @return bool
     */
    function suivireAnimal($animalId)
    {
        return (UserFollows::where('animal_id', $animalId)->where('user_id', Auth::user()->id)->count() > 0) ? true : false;
    }
}