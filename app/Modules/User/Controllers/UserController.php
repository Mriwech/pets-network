<?php

namespace App\Modules\User\Controllers;

use App\Mail\SendNewPasswordUser;
use App\Mail\SendUserForgetPassword;
use App\Mail\SendUserValidation;
use App\Modules\Animal\Models\Animal;
use App\Modules\Photo\Models\Photo;
use App\Modules\User\Models\PasswordReset;
use App\Modules\User\Models\User;
use App\Modules\User\Models\UserFollows;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use UxWeb\SweetAlert\SweetAlert;

class UserController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function handleUserRegistration(Request $request){

        $request->validate([
            'nom' => 'required',
            'prenom' => 'required',
            'email' => 'required|unique:users',
            'password' => 'required',
        ]);

        $user = User::create([
            'nom' => $request->nom,
            'prenom' => $request->prenom,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'validation_token' => Str::random(32)
        ]);

        SweetAlert::success('Veuillez consulter votre adresse email !', 'Compte créé avec succès')->persistent('Fermer');

         try {
             Mail::to($user->email)->send(new SendUserValidation($user));
         } catch (Exception $e) {
             return back();
         }
        return redirect(route('showHomePage', ['param' => 'login']));
    }

    public function handleValidateUserAccount($token){
        $user = User::where('validation_token', $token)
            ->where('email_verified_at', null)
            ->first();
        if(isset($user)){
            $user->email_verified_at = Carbon::now();
            $user->save();
            SweetAlert::success('Votre compte est validé !', 'Compte validé')->persistent('Fermer');
        }
        return redirect(route('showHomePage', ['param' => 'login']));
    }

    public function handleLoginUser(Request $request){

        //Check if user already login
        if(Auth::user()){
            return redirect(route('showUserAccueil'));
        }

        //Check if user email existe in database and if user already have a verified account
        $user = User::where('email', $request->email)->first();

        if(isset($user)){
            if($user->email_verified_at == null){
                SweetAlert::error('Merci de valider votre compte !', 'Compte non validé')->persistent('Fermer');
            }else{
                if (auth()->attempt(['email' => $request->email, 'password' => $request->password], true)) {
                    SweetAlert::success('Connection effectuer avec succèes !', 'Connecté');
                    return redirect(route('showUserAccueil'));
                }else  SweetAlert::error('Incorrect Email or Password !', 'Erreur');
            }
        }else{
            SweetAlert::error('Compte inexistant !', 'Créez un compte avant de se connecter :)')->persistent('Fermer');
            return redirect()->route('showHomePage');
        }
        return redirect()->route('showHomePage', ['param' => 'login']);
    }

    public function handleLogoutUser(){
        Auth::logout();
        return redirect()->route('showHomePage', ['param' => 'login']);
    }

    public function handleUserForgetPassword(Request $request){
        //Check if user already login
        if(Auth::user()){
            return redirect(route('showUserAccueil'));
        }

        //Check if user email existe in database and if user already have a verified account
        $user = User::where('email', $request->email)->first();

        if(isset($user)){
            $password = PasswordReset::create([
                'email' => $user->email,
                'token' => Str::random(32)
            ]);
            try {
                Mail::to($password->email)->send(new SendUserForgetPassword($password));
            } catch (Exception $e) {
                return back();
            }
            SweetAlert::success('Un email de rénitialisation de mot de passe est envoyé vers votre adresse email.', 'Success');
            return redirect()->route('showHomePage', ['param' => 'login']);
        }else{
            SweetAlert::error('Email inexistant !', 'Erreur');
            return redirect()->route('showHomePage', ['param' => 'login']);
        }
    }

    public function handleUserPasswordReset($email, $token){
        $password = PasswordReset::where('email', $email)
            ->where('token', $token)
            ->first();

        if(isset($password) and (Carbon::now()->diffInHours($password->created_at) <= 24)){
                $newPass = Str::random(10);
                $user = User::where('email', $password->email)->first();

                ($user->email_verified_at == null)
                    ? $user->update(['password' => bcrypt($newPass), 'email_verified_at' => Carbon::now()])
                    : $user->update(['password' => bcrypt($newPass)]);


                Mail::to($password->email)->send(new SendNewPasswordUser($newPass));

                PasswordReset::where('email', $email)->where('token', $token)->delete();

                SweetAlert::success('Votre nouveau mot de passe sera envoyer dans votre email !', 'Success');
                return redirect()->route('showHomePage', ['param' => 'login']);
        }else{
            SweetAlert::error('Lien expirée !', 'Erreur');
            return redirect()->route('showHomePage', ['param' => 'login']);
        }
    }

    public function showUserAccueil(){
        $idAnimaux = Auth::user()->follows()->get()->pluck('id');
        $photos = Photo::whereHas('animaux', function ($query) use ($idAnimaux){
            $query->whereIn('id', $idAnimaux);
        })->inRandomOrder()->limit(10)->get();

        return view('User::userAccueil', ['photos' => $photos]);
    }

    public function showUserUpdateProfile($id){
        return view('User::userUpdateProfile', ['user' => User::find($id)]);
    }

    public function handleUserUpdateProfile(Request $request){

        $request->validate([
            'nom' => 'required',
            'prenom' => 'required'
        ]);

        $user = User::where('id', Auth::user()->id)->where('email', $request->email)->first();

        if(isset($user)){
            ($user->nom != $request->nom) ? $user->nom = $request->nom : null;
            ($user->prenom != $request->prenom ) ? $user->prenom = $request->prenom : null;


            if((isset($request->password)) and ($request->password === $request->confPassword)){
                $user->password = bcrypt($request->password);
            }elseif(isset($request->password)){
                SweetAlert::error('Les deux mot de passe sont incohérentes', 'Erreur');
                $user->save();
                return back();
            }
            $user->save();
            SweetAlert::success('Modification effectué avec succées !', 'Modifié');
        }
        return back();
    }

    public function handleUserFollowAnimal($animalId){
        $animal = Animal::find($animalId);
        $userFollow = UserFollows::where('animal_id', $animalId)->where('user_id', Auth::user()->id)->count();

        if($userFollow == 0){
            if((isset($animal)) and ($animal->user_id != Auth::user()->id)){
                Auth::user()->follows()->attach($animalId);
            }else{
                SweetAlert::error('Un erreur est servue!', 'Compte créé avec succès')->persistent('Fermer');
            }
            SweetAlert::success('Vous suivre maintenant '.$animal->nom.' !', 'Succès')->persistent('Fermer');
        }else{
            SweetAlert::error('Vous suivre déja '.$animal->nom.' !', 'Animal dèja suivie')->persistent('Fermer');
        }
        return back();

    }

    public function handleUserUnfollowAnimal($animalId){

        $animal = Animal::find($animalId);
        $userFollow = UserFollows::where('animal_id', $animalId)->where('user_id', Auth::user()->id)->count();

        if($userFollow > 0){
            if((isset($animal)) and ($animal->user_id != Auth::user()->id)){
                Auth::user()->follows()->detach($animalId);
                SweetAlert::success('Vous étes désormais désabonné de suivre '.$animal->nom.' !', 'Succès')->persistent('Fermer');
            }else{
                SweetAlert::error('Un erreur est servue!', 'Erreur')->persistent('Fermer');
            }
        }else{
            SweetAlert::error('Vous ête dèja désabonné de '.$animal->nom.' !', 'Vous n\'êtes pas abonnée')->persistent('Fermer');
        }
        return back();

    }

    public function showUserFollows(){
        return view('User::userFollows');
    }

}
