<?php

namespace App\Modules\User\Models;

use App\Http\Middleware\Authenticate;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;


class UserFollows extends Authenticatable
{

    use Notifiable;

    public $timestamps = true;

    protected $table = 'user_abonnements';

    protected $fillable = ['user_id','animal_id'];
}