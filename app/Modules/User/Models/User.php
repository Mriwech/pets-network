<?php

namespace App\Modules\User\Models;

use App\Http\Middleware\Authenticate;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;


class User extends Authenticatable
{

    use Notifiable;

    public $timestamps = true;

    protected $table = 'users';

    protected $fillable = ['nom','prenom','email','email_verified_at','password', 'validation_token', 'image_path'];

    protected $hidden = ['password', 'remember_token',];


    public function animaux()
    {
        return $this->hasMany('App\Modules\Animal\Models\Animal', 'user_id');
    }

    public function photos()
    {
        return $this->hasMany('App\Modules\Photo\Models\Photo', 'user_id')->orderBy('created_at', 'DESC');
    }

    public function follows()
    {
        return $this->belongsToMany('App\Modules\Animal\Models\Animal', 'user_abonnements', 'user_id', 'animal_id')->withTimestamps();
    }

}