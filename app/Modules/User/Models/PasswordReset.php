<?php

namespace App\Modules\User\Models;

use App\Http\Middleware\Authenticate;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class PasswordReset extends Model
{


    public $timestamps = true;
    const UPDATED_AT = null;


    protected $table = 'password_resets';

    protected $fillable = ['email','token'];


}