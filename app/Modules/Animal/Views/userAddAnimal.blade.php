@extends('frontOffice.backEnd.layout')


@section('content')

    <div class="col-md-7">

        <!-- Basic Information
        ================================================= -->
        <div class="edit-profile-container">
            <div class="block-title">
                <h4 class="grey"><i class="icon ion-android-checkmark-circle"></i>Ajouter un animal</h4>
                <div class="line"></div>

            </div>
            <div class="edit-block">
                <form name="basic-info" id="basic-info" class="form-inline" enctype="multipart/form-data" action="{{route('handleUserAddAnimal')}}" method="post">
                    @csrf
                    <div class="row">
                        <div class="form-group col-xs-8">
                            <label for="nom">Nom</label>
                            <input id="firstname" class="form-control input-group-lg" type="text" name="nom" title="Nom" placeholder="Nom" required>
                        </div>
                    </div>
                    <div class="row">
                            <span class="custom-label"><strong>Sex : </strong></span>
                            <label class="radio-inline">
                                <input type="radio" name="sex" checked value="Masculin">Male
                            </label>
                            <label class="radio-inline">
                                <input type="radio" value="Féminin" name="sex">Female
                            </label>

                    </div>
                    <div class="row">
                        <div class="form-group col-xs-8">
                            <label for="nom">Anniversaire</label>
                            <input class="form-control input-group-lg" type="date" name="birthday" title="Anniversaire" placeholder="Anniversaire" required>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-xs-8">
                            <label for="nom">Image</label>
                            <input class="form-control input-group-lg" type="file" name="image_path" title="image" placeholder="Image" required>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-xs-6">
                            <label for="country">Types</label>
                            <select class="form-control" name="animal" >
                                <option value="Chiens">Chiens</option>
                                <option value="Chats">Chats</option>
                                <option value="Rongeurs">Rongeurs</option>
                                <option value="Reptiles">Reptiles</option>
                                <option value="Poissons">Poissons</option>
                                <option value="Oiseaux">Oiseaux</option>
                                <option value="Chevaux">Chevaux</option>
                                <option value="Invertebrés">Invertebrés</option>
                                <option value="Les autres">Les autres</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-xs-12">
                            <label for="my-info">Description</label>
                            <textarea id="my-info" name="description" class="form-control" placeholder="Quelques information sur votre animal" rows="4" cols="400"></textarea>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Ajouter</button>
                </form>
            </div>
        </div>
    </div>

@endsection





