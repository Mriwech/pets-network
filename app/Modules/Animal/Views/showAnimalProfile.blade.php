@extends('frontOffice.backEnd.layoutProfile')


@section('content')

    <div class="col-md-7">

        <!-- Post Content
        ================================================= -->
        @foreach($animal->photos()->orderBy('created_at', 'DESC')->get() as $photo)
        <div class="post-content">

            <!--Post Date-->
            <div class="post-date hidden-xs hidden-sm">
                <h5>{{ $animal->proprietaire->prenom }}</h5>
                <p class="text-grey">{{ \Carbon\Carbon::now()->diffForHumans($photo->created_at) }}</p>
            </div><!--Post Date End-->
            <img src="{{ asset($photo->image_path) }}" alt="{{$photo->titre}}" class="img-responsive post-image" />
            <div class="post-container">
                <img src="{{asset($animal->proprietaire->image_path)}}" alt="{{$animal->proprietaire->nom}}" class="profile-photo-md pull-left" />
                <div class="post-detail">
                    <div class="user-info">
                        <h5><a class="profile-link">{{$animal->proprietaire->nom}}, {{ $animal->proprietaire->prenom }}</a>
                            <span class="following">following</span></h5>
                        <p class="text-muted">{{ \Carbon\Carbon::now()->diffForHumans($photo->created_at) }}</p>
                    </div>
                    <div class="reaction">
                        <a class="btn text-green"><i class="icon ion-thumbsup"></i> 13</a>
                        <a class="btn text-red"><i class="fa fa-thumbs-down"></i> 0</a>
                    </div>
                    <div class="line-divider"></div>
                    <div class="post-text">
                        <p>{{ $photo->description }} <i class="em em-anguished"></i> <i class="em em-anguished"></i> <i class="em em-anguished"></i></p>
                    </div>
                    @if($photo->commentaires()->count() > 0)
                    <div class="line-divider"></div>
                    @endif
                    @foreach($photo->commentaires()->get() as $commentaire)
                        <div class="post-comment">
                            <img src="{{ asset($commentaire->proprietaire->image_path) }}" alt="{{ $commentaire->proprietaire->nom}}" class="profile-photo-sm" />
                            <p>
                                <a class="profile-link">{{ $commentaire->proprietaire->nom .', '.$commentaire->proprietaire->prenom }}</a>
                                &nbsp;<span class="text-primary">[{{ \Carbon\Carbon::now()->diffForHumans($commentaire->created_at) }}]</span>
                                <br/>
                                <i class="em em-laughing"></i> {{ $commentaire->commentaire }}

                            </p>



                        </div>
                    @endforeach
                    <div class="line-divider"></div>
                    <div class="post-comment">
                        <img src="{{ asset(Auth::user()->image_path) }}" alt="" class="profile-photo-sm" />
                        <form name="commentaire" class="form-inline" action="{{route('handleAddCommentaire')}}" method="post" style="width: 100%">
                            @csrf
                            <div class="row">
                                <div class="form-group col-xs-12">
                                    <input type="hidden" class="form-control" name="photoId" value="{{$photo->id}}">
                                    <input type="text" class="form-control" placeholder="Post a comment" name="commentaire" style="width: 100%;" required>
                                </div>
                                <button type="submit" class="btn btn-primary" style="float: right;">Comment</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>

@endsection





