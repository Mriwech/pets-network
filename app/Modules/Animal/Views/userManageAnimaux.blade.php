@extends('frontOffice.backEnd.layout')


@section('content')

    <div class="col-md-7">

        <!-- Basic Information
        ================================================= -->
        <div class="friend-list">
            <div class="block-title">
                <h4 class="grey"><i class="icon ion-android-checkmark-circle"></i> Mes animaux</h4>
                <div class="line"></div>
            </div>
            @foreach($animaux as $key => $animal)
                @if($key % 2 == 0) <div class="row"> @endif
                    <div class="col-md-6 col-sm-6">
                        <div class="friend-card">
                            <img src="{{asset('/').$animal->image_path}}" alt="{{$animal->name}}" class="img-responsive cover" style="max-width: 286px;max-height: 192px;">
                            <div class="card-info">
                                <div class="friend-info">
                                    <a class="pull-right text-green">{{$animal->animal}}</a>
                                    <h5><a href="{{route('showAnimalProfile', ['id' => $animal->id])}}" class="profile-link">{{$animal->nom}}</a></h5>
                                    {{ \Carbon\Carbon::now()->diffInYears($animal->birthday) }} ans ({{ \Carbon\Carbon::now()->diffInMonths($animal->birthday) }} mois)
                                    <br>
                                    {{--TODO : create function to soft delete animal--}}
                                    <a href="#"><i class="icon ion-android-delete" style="font-size: 20px;"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                @if(($key+1 == count($animaux)) or ($key % 2 == 1)) </div> @endif
            @endforeach

            <a href="{{route('showUserAddAnimal')}}"><button>Ajouter un animal</button></a>

        </div>
    </div>

@endsection





