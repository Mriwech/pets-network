<?php

namespace App\Modules\Animal\Models;

use Illuminate\Database\Eloquent\Model;

class Animal extends Model {

    public $timestamps = true;

    protected $table = 'animaux';

    protected $fillable = ['nom','sex','birthday','animal', 'description','image_path','user_id'];

    public function photos()
    {
        return $this->belongsToMany('App\Modules\Photo\Models\Photo', 'animal_photos','animal_id', 'photo_id')->withTimestamps();
    }

    public function proprietaire()
    {
        return $this->hasOne('App\Modules\User\Models\User', 'id', 'user_id');
    }

}
