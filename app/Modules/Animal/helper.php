<?php

/**
 *	Animal Helper
 */

use App\Modules\Animal\Models\Animal;
use App\Modules\User\Models\UserFollows;
use Illuminate\Support\Facades\Auth;

if (!function_exists('animauxAleatoire')) {
    /**
     * @param \App\Modules\User\Models\User $user
     * @return bool
     */
    function animauxAleatoire()
    {
        return Animal::where('user_id', '!=', Auth::user()->id)
            ->inRandomOrder()
            ->limit(10)->get();
    }
}

if (!function_exists('nbrOfFollowers')) {
    /**
     * @param \App\Modules\User\Models\User $user
     * @return bool
     */
    function nbrOfFollowers($animalId)
    {
        return UserFollows::where('animal_id', $animalId)->count();
    }
}
