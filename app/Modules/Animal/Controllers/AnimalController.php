<?php

namespace App\Modules\Animal\Controllers;

use App\Modules\Animal\Models\Animal;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use UxWeb\SweetAlert\SweetAlert;

class AnimalController extends Controller
{

    public function showUserManageAnimaux(){
        return view('Animal::userManageAnimaux', ['animaux' => Auth::user()->animaux()->get()]);
    }

    public function showUserAddAnimal(){
        return view('Animal::userAddAnimal');
    }

    public function handleUserAddAnimal(Request $request){

        if(isset($request->image_path)) {
            if (!in_array(strtolower($request->image_path->getClientOriginalExtension()), ['png', 'jpeg', 'jpg', 'gif'])) {
                SweetAlert::error('Oops', 'Type d\' image incorrect!')->persistent('Fermer');
                return back();
            }

            $fullImagePath = public_path('images/animaux/');
            $fileName = 'animal-img-'.Auth::user()->id.'-'.time().'.'.$request->image_path->getClientOriginalExtension();

            //Move Uploaded File
            $request->image_path->move($fullImagePath, $fileName);
        }
        //Update dataBase
        Animal::create([
            'nom' => $request->nom,
            'sex' => $request->sex,
            'birthday' => $request->birthday,
            'animal' => $request->animal,
            'description' => $request->description,
            'image_path' => (isset($fileName)) ? 'images/animaux/' . $fileName : null,
            'user_id' => Auth::user()->id

        ]);

        return redirect(route('showUserManageAnimaux'));


    }

    public function showAnimalProfile($id){
        $animal = Animal::find($id);
        if(isset($animal)){
            return view('Animal::showAnimalProfile', ['animal' => $animal]);
        }else{
            return back();
        }
    }

}
