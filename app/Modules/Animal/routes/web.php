<?php

Route::group(['module' => 'Animal', 'middleware' => ['web'], 'namespace' => 'App\Modules\Animal\Controllers'], function() {

    Route::resource('Animal', 'AnimalController');

    Route::prefix('animal')->group(function() {
        Route::get('manage', 'AnimalController@showUserManageAnimaux')->name('showUserManageAnimaux')->middleware('authenticate');
        Route::get('profile/{id}','AnimalController@showAnimalProfile')->name('showAnimalProfile')->middleware('authenticate');
        Route::get('add', 'AnimalController@showUserAddAnimal')->name('showUserAddAnimal')->middleware('authenticate');
        Route::post('add', 'AnimalController@handleUserAddAnimal')->name('handleUserAddAnimal')->middleware('authenticate');
    });
});
