<?php

Route::group(['module' => 'Animal', 'middleware' => ['api'], 'namespace' => 'App\Modules\Animal\Controllers'], function() {

    Route::resource('Animal', 'AnimalController');

});
