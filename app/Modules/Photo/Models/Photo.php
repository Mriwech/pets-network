<?php

namespace App\Modules\Photo\Models;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model {

    public $timestamps = true;

    protected $table = 'photos';

    protected $fillable = ['titre','image_path','description','user_id'];

    public function animaux()
    {
            return $this->belongsToMany('App\Modules\Animal\Models\Animal', 'animal_photos', 'photo_id', 'animal_id')->withTimestamps();
    }

    public function commentaires(){
        return $this->hasMany('App\Modules\Commentaire\Models\Commentaire', 'photo_id')->orderBy('created_at', 'ASC')->where('etat', 1);
    }

    public function commentairesInvalid(){
        return $this->hasMany('App\Modules\Commentaire\Models\Commentaire', 'photo_id')->orderBy('created_at', 'ASC')->where('etat', 0);
    }

    public function allCommentaires(){
        return $this->hasMany('App\Modules\Commentaire\Models\Commentaire', 'photo_id')->orderBy('created_at', 'ASC');
    }

}
