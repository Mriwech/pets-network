<?php

namespace App\Modules\Photo\Controllers;

use App\Modules\Photo\Models\Photo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use UxWeb\SweetAlert\SweetAlert;

class PhotoController extends Controller
{

    public function showUserImages(){
        return view('Photo::userImages');
    }

    public function showPhotoAdd(){
        return view('Photo::userAddPhoto');
    }

    public function handleUserAddPhoto(Request $request){

        if(isset($request->image_path)){
            if (!in_array(strtolower($request->image_path->getClientOriginalExtension()), ['png', 'jpeg', 'jpg', 'gif'])) {
                SweetAlert::error('Oops', 'Type d\' image incorrect!')->persistent('Fermer');
                return back();
            }

            $fullImagePath = public_path('images/photos/');
            $fileName = 'photo-'.Auth::user()->id.'-'.time().'.'.$request->image_path->getClientOriginalExtension();

            //Move Uploaded File
            $request->image_path->move($fullImagePath, $fileName);
        }

        $photo = Photo::create([
            'titre' => $request->titre,
            'image_path' => (isset($fileName)) ? 'images/photos/' . $fileName : null,
            'description' => $request->description,
            'user_id' => Auth::user()->id
        ]);

        if(isset($request->animaux))
            foreach ($request->animaux as $animal){
                $photo->animaux()->attach($animal);
            }


        return redirect(route('showUserAccueil'));
    }
}
