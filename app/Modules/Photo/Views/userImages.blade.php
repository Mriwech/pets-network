@extends('frontOffice.backEnd.layout')


@section('content')

<div class="col-md-7">

    <!-- Post Content
    ================================================= -->
    @foreach(Auth::user()->photos()->get() as $photo)
    <div class="post-content">
        <img src="{{asset($photo->image_path)}}" alt="post-image" class="img-responsive post-image" />
        <div class="post-container">
            <div class="post-detail" style="margin-left: 0;">
                <div class="user-info">
                    <h5>
                        <a href="#" class="profile-link">{{ $photo->titre }}</a>
                        {{--<span class="following">following</span>--}}
                    </h5>
                    {{--<p class="text-muted">Published a photo about 3 mins ago</p>--}}
                    <p class="text-muted">{{ \Carbon\Carbon::now()->diffForHumans($photo->created_at) }}</p>
                    <b>Tags :</b>
                    @foreach($photo->animaux()->get() as $animal)
                        <a href="{{ route('showAnimalProfile', ['id' => $animal->id]) }}" class="profile-link">{{ $animal->nom }}</a>&nbsp;
                    @endforeach
                </div>
                <div class="reaction">
                    <a class="btn text-green"><i class="icon ion-thumbsup"></i> 13</a>
                </div>
                <div class="line-divider"></div>
                <div class="post-text">
                    <p>
                        {{ $photo->description }}
                        <i class="em em-anguished"></i> <i class="em em-anguished"></i> <i class="em em-anguished"></i>
                    </p>
                </div>
                {{--<div class="line-divider"></div>--}}
                {{--<div class="post-comment">--}}
                    {{--<img src="http://placehold.it/300x300" alt="" class="profile-photo-sm" />--}}
                    {{--<p><a href="timeline.html" class="profile-link">Diana </a><i class="em em-laughing"></i> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud </p>--}}
                {{--</div>--}}
                {{--<div class="post-comment">--}}
                    {{--<img src="http://placehold.it/300x300" alt="" class="profile-photo-sm" />--}}
                    {{--<p><a href="timeline.html" class="profile-link">John</a> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud </p>--}}
                {{--</div>--}}
                {{--<div class="post-comment">--}}
                    {{--<img src="http://placehold.it/300x300" alt="" class="profile-photo-sm" />--}}
                    {{--<input type="text" class="form-control" placeholder="Post a comment">--}}
                {{--</div>--}}
            </div>
        </div>
    </div>
    @endforeach
    <a href="{{route('showPhotoAdd')}}"><button>Ajouter un image</button></a>
</div>

@endsection





