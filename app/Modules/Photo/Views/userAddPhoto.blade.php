@extends('frontOffice.backEnd.layout')


@section('content')

    <div class="col-md-7">

        <!-- Basic Information
        ================================================= -->
        <div class="edit-profile-container">
            <div class="block-title">
                <h4 class="grey"><i class="icon ion-android-checkmark-circle"></i>Ajouter une photo</h4>
                <div class="line"></div>
            </div>
            <div class="edit-block">
                <form name="basic-info" id="basic-info" class="form-inline" enctype="multipart/form-data" action="{{route('handleUserAddPhoto')}}" method="post">
                    @csrf
                    <div class="row">
                        <div class="form-group col-xs-8">
                            <label for="nom">Titre</label>
                            <input id="firstname" class="form-control input-group-lg" type="text" name="titre" title="titre" placeholder="Titre" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-xs-8">
                            <label for="nom">Image</label>
                            <input class="form-control input-group-lg" type="file" name="image_path" title="image" placeholder="Image" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-xs-12">
                            <label for="my-info">Description</label>
                            <textarea id="my-info" name="description" class="form-control" placeholder="Quelques information sur votre animal" rows="4" cols="400" required></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-xs-6">
                            <label for="country">Animaux tags</label>
                            <select class="js-example-basic-multiple col-xs-8" name="animaux[]" multiple="multiple" required>
                                @foreach(Auth::user()->animaux()->get() as $animal)
                                <option value="{{$animal->id}}">{{$animal->nom}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Ajouter</button>
                </form>
            </div>
        </div>
    </div>

@endsection
@section('js')
    <script>
        $(document).ready(function() {
            $('.js-example-basic-multiple').select2();
        });
    </script>
@endsection





