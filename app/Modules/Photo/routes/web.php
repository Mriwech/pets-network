<?php

Route::group(['module' => 'Photo', 'middleware' => ['web'], 'namespace' => 'App\Modules\Photo\Controllers'], function() {

    Route::resource('Photo', 'PhotoController');

    Route::prefix('photo')->group(function() {

        Route::get('show', 'PhotoController@showUserImages')->name('showUserImages')->middleware('authenticate');
        Route::get('add', 'PhotoController@showPhotoAdd')->name('showPhotoAdd')->middleware('authenticate');
        Route::post('add', 'PhotoController@handleUserAddPhoto')->name('handleUserAddPhoto')->middleware('authenticate');
    });

});
