@extends('frontOffice.frontEnd.layout')

@section('header')
    <!-- Header================================================= -->
    <header id="header-inverse">
        <nav class="navbar navbar-default navbar-fixed-top menu">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index-register.html"><img src="images/logo.png" alt="logo" /></a>
                </div>
            </div><!-- /.container -->
        </nav>
    </header>
    <!--Header End-->
@endsection
@section('content')
    <!-- Landing Page Contents================================================= -->
    <div id="lp-register">
        <div class="container wrapper">
            <div class="row">
                <div class="col-sm-5">
                    <div class="intro-texts">
                        <h1 class="text-white">Make Cool Friends !!!</h1>
                        <p>Friend Finder is a social network template that can be used to connect people. The template offers Landing pages, News Feed, Image/Video Feed, Chat Box, Timeline and lot more. <br /> <br />Why are you waiting for? Buy it now.</p>
                        <button class="btn btn-primary">Learn More</button>
                    </div>
                </div>
                <div class="col-sm-6 col-sm-offset-1">
                    <div class="reg-form-container">
                        <!-- Register/Login Tabs-->
                        <div class="reg-options">
                            <ul class="nav nav-tabs">
                                <li @if(!isset($param)) class="active" @endif><a href="#register" data-toggle="tab">Inscription</a></li>
                                <li @if(isset($param)) class="active" @endif><a href="#login" data-toggle="tab">Connection</a></li>
                            </ul><!--Tabs End-->
                        </div>

                        <!--Registration Form Contents-->
                        <div class="tab-content">
                            <div class="tab-pane @if(!isset($param)) active @endif" id="register">
                                <h3>Register Now !!!</h3>
                                <p class="text-muted">Be cool and join today. Meet millions</p>
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                {{ $error }}
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                            <!--Register Form-->
                                <form name="registration_form" id='registration_form' role="form" class="form-inline" action="{{route('handleUserRegistration')}}" method="post">
                                        {{ csrf_field() }}
                                    <div class="row">
                                        <div class="form-group col-xs-6">
                                            <label for="nom" class="sr-only">Nom</label>
                                            <input id="firstname" class="form-control input-group-lg" type="text" name="nom" required title="Nom" placeholder="Nom"/>
                                        </div>
                                        <div class="form-group col-xs-6">
                                            <label for="prenom" class="sr-only">Prenom</label>
                                            <input id="lastname" class="form-control input-group-lg" type="text" name="prenom" required title="Prénom" placeholder="Prénom"/>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-xs-12">
                                            <label for="email" class="sr-only">Email</label>
                                            <input id="email" class="form-control input-group-lg" type="text" name="email" required title="Email" placeholder="Votre Email"/>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-xs-12">
                                            <label for="password" class="sr-only">Password</label>
                                            <input id="password" class="form-control input-group-lg" type="password" name="password" required title="Votre password" placeholder="Password"/>
                                        </div>
                                    </div>
                                    <p><a href="{{ route('showHomePage', ['param' => 'login']) }}">Vous avez dèja un compte ?</a></p>
                                    <button type="submit" class="btn btn-primary">S'inscrire</button>
                                </form><!--Register Now Form Ends-->
                            </div><!--Registration Form Contents Ends-->

                            <!--Login-->
                            <div class="tab-pane @if(isset($param)) active @endif" id="login">
                                <h3>Login</h3>
                                <p class="text-muted">Log into your account</p>
                                <!--Login Form-->
                                <form name="Login_form" id='Login_form' action="{{ route('handleLoginUser') }}" method="post">
                                    @csrf
                                    <div class="row">
                                        <div class="form-group col-xs-12">
                                            <label for="my-email" class="sr-only">Email</label>
                                            <input id="my-email" class="form-control input-group-lg" type="text" name="email" title="Enter Email" placeholder="Your Email" value="test1@test.fr"/>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-xs-12">
                                            <label for="my-password" class="sr-only">Password</label>
                                            <input id="my-password" class="form-control input-group-lg" type="password" name="password" title="Enter password" placeholder="Password" value="123456"/>
                                        </div>
                                    </div>
                                    <p><a href="#forget" data-toggle="tab">Forgot Password?</a></p>
                                    <button type="submit" class="btn btn-primary">Login Now</button>
                                </form><!--Login Form Ends-->
                                <br/>
                                <p><b>Demo Account 1 :</b> test1@test.fr / 123456</p>
                                <p><b>Demo Account 2 :</b> test3@test.fr / 123456</p>
                                <p><b>Demo Account 3 :</b> test3@test.fr / 123456</p>
                            </div>

                            <!--Forget password-->
                            <div class="tab-pane" id="forget">
                                <h3>Oublier mot de passe</h3>
                                <p class="text-muted">Donnez votre email</p>

                                <!--Login Form-->
                                <form name="Login_form" id='Login_form' action="{{ route('handleUserForgetPassword') }}" method="post">
                                    @csrf
                                    <div class="row">
                                        <div class="form-group col-xs-12">
                                            <label for="my-email" class="sr-only">Email</label>
                                            <input id="my-email" class="form-control input-group-lg" type="text" name="email" title="Enter Email" placeholder="Your Email"/>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-primary">Envoyer</button>
                                </form><!--Login Form Ends-->

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-sm-offset-6">

                    <!--Social Icons-->
                    <ul class="list-inline social-icons">
                        <li><a href="#"><i class="icon ion-social-facebook"></i></a></li>
                        <li><a href="#"><i class="icon ion-social-twitter"></i></a></li>
                        <li><a href="#"><i class="icon ion-social-googleplus"></i></a></li>
                        <li><a href="#"><i class="icon ion-social-pinterest"></i></a></li>
                        <li><a href="#"><i class="icon ion-social-linkedin"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <!--preloader-->
    <div id="spinner-wrapper">
        <div class="spinner"></div>
    </div>

@endsection





