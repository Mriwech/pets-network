<?php

namespace App\Modules\General\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GeneralController extends Controller
{
    public function showHomePage($param = null){
        return view('General::homePage', ['param' => $param]);
    }


}
