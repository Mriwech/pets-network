<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

# Projet : Pets network

## Présentation géneral  

Ce projet est représente un réseaux sociaux pour les éleveurs d'animaux afin de partager entre eux les images, les commentaires et échanger les informations entre eux.

## Les outils nécessaires 

- [Xampp](https://www.apachefriends.org/fr/index.html) ou [WampServer](http://www.wampserver.com/)
- [Composer](https://getcomposer.org/)
- [GitBash](https://git-scm.com/) / Terminal sous Linux
- Navigateur web ([Google chrome](https://www.google.com/intl/fr/chrome/) / [Firefox](https://www.mozilla.org/fr/firefox/new/))

## Versions
- PHP 7.0
- Laravel 5.8

## IDE
- PHPStorm 2019.1.3 

## Les étapes d'installation

- Clonner le projet dans votre environement avec la commande :
```
git clone git@gitlab.com:Mriwech/pets-network.git
```

- Installer les dépendances du projet avec la commande :
```
cd pets-network
composer install
```

- Consulter le fichier de configuration de la BD sous la racine pets-network\config\database.php et vérifier les configurations (DB_DATABASE, DB_USERNAME et DB_PASSWORD) et par la suite : 

- Crée une base de donnée Mysql sous le nom : _"db_pets_network"_ 
Pour la création des tables avec les seeders lancer la commande : 
```
php artisan migrate --seed
```


- Ouvrire le project maintenant avec le navigateur : http://localhost/pets-network/public/

### Compte de test :

J'ai dèja mis en place un seeder qui permet de remplire la base avec quelques données de test vous pouvez vous connectez avec ces 3 compte ou bien crée un nouveau compte mais il vaut faut un email valide :

```
Compte 1 : 
Email : test1@test.fr 
MDP   : 123456
**************************
Compte 2 : 
Email : test2@test.fr 
MDP   : 123456
**************************
Compte 3 : 
Email : test3@test.fr 
MDP   : 123456
**************************
```



## Les fonctionalités qui sont dèja intègrer : 

- Register / Login / Logout
- Validation de compte par email / Rénitialisation de mot de passe 
- Editer profile
- S'abonner et désabonner 
- Ajouter des animaux
- Ajouter des images 
- Commenter des images
- Gèrer les commentaires

## Réalisateur 
- [Tayari Marouéne](mailto:tayari_marouene@yahoo.fr), Développeur PHP / Laravel
