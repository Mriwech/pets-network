<?php


use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class ForeignKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_abonnements', function(Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('animal_id')->references('id')->on('animaux');
        });

        Schema::table('animaux', function(Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
        });
//
        Schema::table('photos', function(Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
        });

        Schema::table('commentaires', function(Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('photo_id')->references('id')->on('photos');
        });

        Schema::table('animal_photos', function(Blueprint $table) {
            $table->foreign('photo_id')->references('id')->on('photos');
            $table->foreign('animal_id')->references('id')->on('animaux');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
//        Schema::table('animaux', function(Blueprint $table) {
//            $table->dropForeign('animaux_user_id_foreign');
//        });

//        Schema::dropIfExists('user_abonnements');
//        Schema::dropIfExists('animaux');
//        Schema::dropIfExists('photos');
//        Schema::dropIfExists('commentaires');
//        Schema::dropIfExists('animal_photos');
    }
}