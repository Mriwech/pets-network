<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnimauxTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('animaux', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nom');
            $table->enum('sex', ['Masculin', 'Féminin']);
            $table->date('birthday');
            $table->enum('animal', ['Chiens', 'Chats', 'Rongeurs', 'Reptiles', 'Poissons', 'Oiseaux', 'Chevaux', 'Invertebrés', 'Les autres']);
            $table->text('description')->nullable();
            $table->text('image_path');
            $table->unsignedBigInteger('user_id');
            $table->timestamps();
        });

        Schema::create('animal_photos', function(Blueprint $table) {
            $table->unsignedBigInteger('animal_id');
            $table->unsignedBigInteger('photo_id');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('photos');
        Schema::dropIfExists('animal_photos');

    }
}