<?php

use App\Modules\Animal\Models\Animal;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class AnimauxTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Animal::create([
            'nom' => 'Fox',
            'sex' => 'Masculin',
            'birthday' => Carbon::now()->lastOfMonth(5),
            'animal' => 'Chiens',
            'description' => 'Ceci est un exmple d\'un animal',
            'image_path' => 'images/animaux/fox.jpg',
            'user_id' => 1
        ]);

        Animal::create([
            'nom' => 'Horse',
            'sex' => 'Masculin',
            'birthday' => Carbon::now()->lastOfYear(5),
            'animal' => 'Chevaux',
            'description' => 'Ceci est un exmple d\'un animal',
            'image_path' => 'images/animaux/horse.jpg',
            'user_id' => 2
        ]);

        Animal::create([
            'nom' => 'Lion',
            'sex' => 'Féminin',
            'birthday' => Carbon::now()->lastOfQuarter(2),
            'animal' => 'Rongeurs',
            'description' => 'Ceci est un exmple d\'un animal',
            'image_path' => 'images/animaux/lion.jpg',
            'user_id' => 3
        ]);
    }
}
