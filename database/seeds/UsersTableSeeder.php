<?php

use App\Modules\User\Models\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'nom' => 'user',
            'prenom' => '1',
            'email' => 'test1@test.fr',
            'password' => bcrypt(123456),
            'email_verified_at' => \Carbon\Carbon::now(),
            'image_path' => 'images/users/'.mt_rand(0, 5).'.jpg'
        ]);

        User::create([
            'nom' => 'user',
            'prenom' => '2',
            'email' => 'test2@test.fr',
            'password' => bcrypt(123456),
            'email_verified_at' => \Carbon\Carbon::now(),
            'image_path' => 'images/users/'.mt_rand(0, 5).'.jpg'
        ]);

        User::create([
            'nom' => 'user',
            'prenom' => '3',
            'email' => 'test3@test.fr',
            'password' => bcrypt(123456),
            'email_verified_at' => \Carbon\Carbon::now()
        ]);
    }
}
